<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('user')->truncate();
        factory(App\Models\User::class, 1)->create(["name"=>"owner","username"=>"owner@gmail.com", "role" => 'owner', "password" => bcrypt('owner')]);
    }
}
