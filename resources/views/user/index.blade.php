@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Data User
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data User</h3> -->
                        <div class="pull-right box-tools">
                            <form class="form-inline">
                                <!-- <div class='input-group date' id='datepicker'>
                                    <input type='text' readonly="" value="<?php echo date('01-m-Y') ?>" name="filtertanggal" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>&nbsp;&nbsp;<b>-</b>&nbsp;
                                <div class='input-group date' id='datepicker'>
                                    <input type='text' readonly="" value="<?php echo date('t-m-Y') ?>" name="filtertanggal1" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>&nbsp;
                                <button class="btn btn-success btn-sm" type="button" id="btn-excel">Excel</button>-->
                                <a href="{{route('user.create')}}"><button class="btn btn-success btn-sm" type="button">Tambah</button></a>
                            </form>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="table" class="table table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Hak Akses</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($datas as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data->username}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->role}}</td>
                                    <td>
                                        <a href="{{route('user.edit', $data->id)}}"> <i class="fa fa-edit text-warning" title="Edit User"></i></a>
                                        @if ($data->id != 1)
                                            <a data-toggle="modal" data-target="#deleteModal" data-action="{{route('user.destroy', $data->id)}}"> <i class="fa fa-trash-o text-danger" title="Hapus User"></i>  </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
            <div class="modal-dialog" role="document">
                <form method="post" action="" id="deleteModalForm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Hapus User ?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Anda yakin untuk menghapus user ini?
                            </p>
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/user.js" type="text/javascript"></script>
@endsection
