@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Form User
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data User</h3> -->
                        <div class="pull-right box-tools">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                <span>{{session('success')}}</span>
                            </div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{session('error')}}</span>
                            </div>
                        @endif
                        @if (@$user)
                        <form id="form" action="{{route('user.update',$user->id)}}" method="post" role="form">
                        @else
                        <form id="form" action="{{route('user.store')}}" method="post" role="form">
                        @endif
                        @csrf
                        @if(@$user)
                            <input type="hidden" name="_method" value="put" />
                        @endif
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Username </label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$user->username}}" name="username" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Nama Lengkap</label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$user->name}}" name="name" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Password</label>
                                <div class="col-xs-9">
                                <input class="form-control" name="password" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Role</label>
                                <div class="col-xs-9">
                                    <select class="form-control" required="" name="role">
                                        <option value="">Pilih Tipe</option>
                                        <option {{@$user->role == 'owner' ? 'selected' : ''}} value="owner">Owner</option>
                                        <option {{@$user->role == 'admin_gudang' ? 'selected' : ''}} value="admin_gudang">Admin Gudang</option>
                                        <option {{@$user->role == 'admin_penjualan' ? 'selected' : ''}} value="admin_penjualan">Admin Penjualan</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{route('user.index')}}"><button type="button" class="btn btn-default">Batal</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/user.js" type="text/javascript"></script>
@endsection
