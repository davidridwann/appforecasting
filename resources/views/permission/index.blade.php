@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Permintaan Hak Akses Forecasting
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive" id="section-table">
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                <span>{{session('success')}}</span>
                            </div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{session('error')}}</span>
                            </div>
                        @endif
                        <table id="table" class="table table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>User</th>
                                    <th>Status</th>
                                    <th>Tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $index => $data)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{ $data->user->name }}</td>
                                        <td style="text-transform: uppercase">{{ $data->status }}</td>
                                        <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d-m-Y') }}</td>
                                        <td width="20%">
                                            <a class="btn btn-primary" href="{{ route('permission.accepted', $data->id) }}">Terima</a>
                                            <a class="btn btn-danger" href="{{ route('permission.declined', $data->id) }}">Tolak</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/permission.js" type="text/javascript"></script>
@endsection
