@extends('app')
@section('css')
<style>
.file-section {
    padding-top: 50px;
    padding-bottom: 50px;
}

.file-drop-area {
  position: relative;
  display: flex;
  align-items: center;
  width: 450px;
  max-width: 100%;
  padding: 25px;
  border: 1px dashed #2ecc71;
  border-radius: 3px;
  transition: 0.2s;
  margin: auto;
  right: 0;
  left: 0;
  &.is-active {
    background-color: rgba(255, 255, 255, 0.05);
  }
}

.fake-btn {
  flex-shrink: 0;
  background-color: #2ecc71;
  border: 1px solid #2ecc71;
  border-radius: 3px;
  padding: 8px 15px;
  margin-right: 10px;
  font-size: 12px;
  text-transform: uppercase;
  color: white;
  font-weight: bold;
}

#type-import {
  position: absolute;
  display: flex;
  align-items: center;
  width: 200px;
  max-width: 100%;
  border: 1px solid #2ecc71;
  border-radius: 3px;
  padding: 8px 15px;
  margin-right: 10px;
  font-size: 12px;
  text-transform: uppercase;
  color: #2ecc71;
  font-weight: bold;
  margin: auto;
  right: 58%;
  left: 0;
}

#type-import:focus {
    border-color: unset !important;
}

.file-msg {
  font-size: small;
  font-weight: 300;
  line-height: 1.4;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.file-input {
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  cursor: pointer;
  opacity: 0;
  &:focus {
    outline: none;
  }
}

.d-none {
    display:none !important;
}
</style>
@endsection
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Data Barang
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @if (Auth::user()->role != 'admin_penjualan')
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data Barang</h3> -->
                        <div class="pull-right box-tools">
                            <form class="form-inline">
                                <!-- <div class='input-group date' id='datepicker'>
                                    <input type='text' readonly="" value="<?php echo date('01-m-Y') ?>" name="filtertanggal" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>&nbsp;&nbsp;<b>-</b>&nbsp;
                                <div class='input-group date' id='datepicker'>
                                    <input type='text' readonly="" value="<?php echo date('t-m-Y') ?>" name="filtertanggal1" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>&nbsp;
                                <button class="btn btn-success btn-sm" type="button" id="btn-excel">Excel</button>-->
                                <a class="btn btn-warning btn-sm d-none" id="cancel-to-import" style="color:white;">Batal</a>
                                <a class="btn btn-success btn-sm d-none" id="fake-btn" style="color:white;">Submit</a>
                                <a class="btn btn-success btn-sm" id="change-to-import" style="color:white;">Import</a>
                                <a href="{{route('barang.create')}}" id="add-btn"><button class="btn btn-primary btn-sm" type="button">Tambah</button></a>
                            </form>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body d-none" id="section-import">
                        <form id="form" action="{{route('import.barang')}}" method="post" role="form" enctype="multipart/form-data">
                            @csrf
                            <div class="file-section">
                                <div class="file-drop-area">
                                    <span class="fake-btn">Choose files</span>
                                    <span class="file-msg">or drag and drop files here</span>
                                    <input class="file-input" type="file" name="file">
                                </div>
                                @if ($errors->has('file'))
                                    <span role="alert" style="color:#e74c3c;">
                                        <strong>* {{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button class="btn btn-success btn-sm" type="submit" id="real-btn" style="display:none;">Import</button>
                        </form>
                    </div>
                    @endif

                    <div class="box-body table-responsive" id="section-table">
                        <table id="table" class="table table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>SKU</th>
                                    <th>Harga Jual</th>
                                    <th>Harga Beli</th>
                                    <th>Tipe</th>
                                    <th>Stock</th>
                                    @if (Auth::user()->role != 'admin_penjualan')
                                    <th>Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($datas as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data->nama}}</td>
                                    <td>{{$data->sku}}</td>
                                    <td>{{number_format($data->harga_jual,0,"",".")}}</td>
                                    <td>{{number_format($data->harga_beli,0,"",".")}}</td>
                                    <td>{{@$data->tipe->nama}}</td>
                                    <td>{{$data->stock}}</td>
                                    @if (Auth::user()->role != 'admin_penjualan')
                                    <td>
                                        <a href="{{route('barang.edit', $data->id)}}"> <i class="fa fa-edit text-warning" title="Edit Barang"></i></a>
                                        <a data-toggle="modal" data-target="#deleteModal" data-action="{{route('barang.destroy', $data->id)}}"> <i class="fa fa-trash-o text-danger" title="Hapus Barang"></i>  </a>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
            <div class="modal-dialog" role="document">
                <form method="post" action="" id="deleteModalForm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Hapus Barang ?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Anda yakin untuk menghapus barang ini?
                            </p>
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/barang.js" type="text/javascript"></script>
    <script>
        var $fileInput = $('.file-input');
        var $droparea = $('.file-drop-area');

        $fileInput.on('dragenter focus click', function() {
            $droparea.addClass('is-active');
        });

        $fileInput.on('dragleave blur drop', function() {
            $droparea.removeClass('is-active');
        });

        $fileInput.on('change', function() {
            var filesCount = $(this)[0].files.length;
            var $textContainer = $(this).prev();

            if (filesCount === 1) {
                var fileName = $(this).val().split('\\').pop();
                $textContainer.text(fileName);
            } else {
                $textContainer.text(filesCount + ' files selected');
            }
        });

        $('#fake-btn').click(function() {
            $('#real-btn').click();
        })

        $('#change-to-import').click(function() {
            $('#section-table').addClass('d-none');
            $('#change-to-import').addClass('d-none');
            $('#add-btn').addClass('d-none');
            $('#section-import').removeClass('d-none');
            $('#cancel-to-import').removeClass('d-none');
            $('#fake-btn').removeClass('d-none');
        })

        $('#cancel-to-import').click(function() {
            $('#section-import').addClass('d-none');
            $('#fake-btn').addClass('d-none');
            $('#cancel-to-import').addClass('d-none');
            $('#section-table').removeClass('d-none');
            $('#change-to-import').removeClass('d-none');
            $('#add-btn').removeClass('d-none');
        })
    </script>
@endsection
