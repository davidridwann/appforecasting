@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Form Barang
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data barang</h3> -->
                        <div class="pull-right box-tools">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                <span>{{session('success')}}</span>
                            </div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{session('error')}}</span>
                            </div>
                        @endif
                        @if (@$barang)
                        <form id="form" action="{{route('barang.update',$barang->id)}}" method="post" role="form">
                        @else
                        <form id="form" action="{{route('barang.store')}}" method="post" role="form">
                        @endif
                        @csrf
                        @if(@$barang)
                            <input type="hidden" name="_method" value="put" />
                        @endif
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Nama </label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$barang->nama}}" name="nama" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">SKU </label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$barang->sku}}" name="sku" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Harga Jual </label>
                                <div class="col-xs-9">
                                <input class="form-control only-numeric" required="" value="{{@$barang->harga_jual}}" name="harga_jual" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Harga Beli </label>
                                <div class="col-xs-9">
                                <input class="form-control only-numeric" required="" value="{{@$barang->harga_beli}}" name="harga_beli" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Tipe </label>
                                <div class="col-xs-9">
                                    <select class="form-control" required name="tipe_id">
                                        <option value="">Pilih Tipe</option>
                                        @foreach ($tipes as $tipe)
                                            <option @if($tipe->id == @$barang->tipe_id) selected @endif value="{{$tipe->id}}">{{$tipe->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Stock </label>
                                <div class="col-xs-9">
                                <input class="form-control only-numeric" required="" value="{{@$barang->stock}}" name="stock" />
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{route('barang.index')}}"><button type="button" class="btn btn-default">Batal</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/barang.js" type="text/javascript"></script>
@endsection
