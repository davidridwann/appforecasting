@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Laporan Pembelian
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data Barang</h3> -->
                        <div class="pull-right box-tools">
                            <form class="form-inline">
                                <a class="btn btn-primary btn-sm" href="{{ route('pembelian.export.all') }}" type="button" style="color:white;">Cetak Semua</a>
                                <a class="btn btn-info btn-sm" id="export-fake" type="button" style="color:white;">Cetak Pilihan</a>
                                <a href="{{route('laporan_pembelian.create')}}"><button class="btn btn-success btn-sm" type="button">Tambah</button></a>
                            </form>
                        </div>
                    </div><!-- /.box-header -->
                    <form id="form" action="{{route('pembelian.export')}}" method="get" role="form">
                    <button class="btn btn-success btn-sm" id="export-real" type="submit" style="display:none;">Cetak Dokumen Real</button>
                        <div class="box-body table-responsive">
                            <table id="table" class="table table-bordered table-hover" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Pilih</th>
                                        <th>Nama Barang</th>
                                        <th>SKU</th>
                                        <th>Harga Beli</th>
                                        <th>Supplier</th>
                                        <th>Qty</th>
                                        <th>Total</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td class="text-center"><input class="form-control" type="checkbox" value="{{@$data->id}}" name="exports[]" /></td>
                                        <td>{{$data->barang->nama}}</td>
                                        <td>{{$data->barang->sku}}</td>
                                        <td>{{number_format($data->harga_beli,0,"",".")}}</td>
                                        <td>{{@$data->supplier->nama}}</td>
                                        <td>{{$data->qty}}</td>
                                        <td>{{ number_format($data->harga_beli * $data->qty,0,"",".")}}</td>
                                        <td>
                                            <a href="{{route('laporan_pembelian.edit', $data->id)}}"> <i class="fa fa-edit text-warning" title="Edit Data Pembelian"></i></a>
                                            <a data-toggle="modal" data-target="#deleteModal" data-action="{{route('laporan_pembelian.destroy', $data->id)}}"> <i class="fa fa-trash-o text-danger" title="Hapus Data Pembelian"></i>  </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
            <div class="modal-dialog" role="document">
                <form method="post" action="" id="deleteModalForm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Hapus Data Pembelian ?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Anda yakin untuk menghapus data pembelian ini?
                            </p>
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/pembelian.js" type="text/javascript"></script>
    <script>
        $('#export-fake').click(function() {
            $('#export-real').click();
        })
    </script>
@endsection
