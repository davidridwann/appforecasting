@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Form Pembelian
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                <span>{{session('success')}}</span>
                            </div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{session('error')}}</span>
                            </div>
                        @endif
                        @if (@$pembelian)
                        <form id="form" action="{{route('laporan_pembelian.update',$pembelian->id)}}" method="post" role="form">
                        @else
                        <form id="form" action="{{route('laporan_pembelian.store')}}" method="post" role="form">
                        @endif
                        @csrf
                        @if(@$pembelian)
                            <input type="hidden" name="_method" value="put" />
                        @endif
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Barang </label>
                                <div class="col-xs-9">
                                    @if(@$pembelian)
                                        <select class="form-control" required name="barang_id" readonly>
                                            <option value="">Pilih Barang</option>
                                            @foreach ($items as $item)
                                                <option @if($item->id == @$pembelian->barang_id) selected @endif value="{{$item->id}}">{{$item->nama}}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select class="form-control" required name="barang_id">
                                            <option value="">Pilih Barang</option>
                                            @foreach ($items as $item)
                                                <option @if($item->id == @$pembelian->barang_id) selected @endif value="{{$item->id}}">{{$item->nama}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Supplier </label>
                                <div class="col-xs-9">
                                    <select class="form-control" required name="supplier_id">
                                        <option value="">Pilih Supplier</option>
                                        @foreach ($suppliers as $supplier)
                                            <option @if($supplier->id == @$pembelian->supplier_id) selected @endif value="{{$supplier->id}}">{{$supplier->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Harga Beli </label>
                                <div class="col-xs-9">
                                <input class="form-control only-numeric" required="" value="{{@$pembelian->harga_beli}}" name="harga_beli" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Qty </label>
                                <div class="col-xs-9">
                                @if (@$pembelian)
                                    <input class="form-control only-numeric" required="" value="{{@$pembelian->qty}}" name="qty" readonly/>
                                @else
                                    <input class="form-control only-numeric" required="" value="{{@$pembelian->qty}}" name="qty" />
                                @endif
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{route('laporan_pembelian.index')}}"><button type="button" class="btn btn-default">Batal</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/barang.js" type="text/javascript"></script>
@endsection
