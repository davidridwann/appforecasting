@extends('app')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Home
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <center><h1><b>Selamat Datang di CV. Perkakas Indonesia</b></h1></center>
        <div class="pull-left box-tools">
            <form class="form-inline" style="margin-bottom: 10px;">
                <div class="form-group row">
                    <div class="col-xs-9">
                        <select name="type" id="" class="form-control">
                            @if (!empty(request()->get('type')))
                                <option value="day" {{ request()->get('type') == 'day' ? 'selected' : '' }}>Hari</option>
                                <option value="month" {{ request()->get('type') == 'month' ? 'selected' : '' }}>Bulan</option>
                            @else
                                <option value="day">Hari</option>
                                <option value="month">Bulan</option>
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-success btn-sm" type="submit">Lihat</button>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <canvas id="myChart" width="400" height="200"></canvas>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
<script>
    $(document).ready(function() {
	$('.menuhome').addClass("active");
} );

var ctx = document.getElementById("myChart").getContext('2d');
var getLabels = @json($object);
var getValues = @json($values);

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: getLabels,
        datasets: [{
            label: 'Total Penjualan',
            data: getValues,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
@endsection
