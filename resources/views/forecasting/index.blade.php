@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Forecasting
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @if ($forecastingPermission)
                        <div class="box-header">
                            <!-- <h3 class="box-title">Data Barang</h3> -->
                            <div class="pull-left box-tools">
                                <form class="form-inline">
                                    <div class="form-group row">
                                        <div class="col-xs-9">
                                            <input class="form-control form-control-sm datepicker" required="" name="dari" placeholder="Dari Bulan" value="{{ @request()->dari }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-9">
                                            <input class="form-control form-control-sm datepicker" required="" name="sampai" placeholder="Sampai Bulan" value="{{ @request()->sampai }}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success btn-sm" type="submit">Cek</button>
                                    </div>
                                </form>
                                @if (!empty(request()->get('dari')))
                                    @if ($status == false)
                                        @if ($monthValidation)
                                            <span class="text-danger">* Tidak boleh kurang dari 7 bulan</span>
                                        @else
                                            <span class="text-danger">* Tanggal terakhir yg dipilih lebih besar dari pada data tanggal terakhir penjualan</span>
                                        @endif
                                    @endif
                                @endif
                            </div>
                            <div class="pull-right box-tools">
                                <form class="form-inline">
                                    <div class="form-group row">
                                        <div class="col-xs-9">
                                            <input class="form-control form-control-sm datepicker" type="hidden" required="" name="dari" placeholder="Dari Bulan" value="{{ @request()->dari }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-9">
                                            <input class="form-control form-control-sm datepicker" type="hidden" required="" name="sampai" placeholder="Sampai Bulan" value="{{ @request()->sampai }}" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-9">
                                            <input class="form-control form-control-sm datepicker" name="forecasting" placeholder="Forecasting" value="{{ @request()->forecasting }}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success btn-sm" type="submit">Cek</button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                        <table id="table" class="table table-bordered table-hover" style="width: 100%;">
                            <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Periode</th>
                                        <th>Nama Barang</th>
                                        <th>Qty Penjualan</th>
                                        <th>Simple Moving Average {{ @request()->forecasting ? $periode : '-' }} Periode / Produk</th>
                                        <th>SE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($status)
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($datas as $data)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$data->periode}}</td>
                                                <td>{{$data->nama}}</td>
                                                <td>{{$data->totalQty}}</td>
                                                <td class="text-bold">{{ $data->forecasting_final }}</td>
                                                <td class="text-bold">{{ $data->se }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td><b>MSE</b></td>
                                        <td><b>{{ $mse }}</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td><b>RMSE</b></td>
                                        <td><b>{{ $rmse }}</b></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    @else
                    <div style="padding-top: 10rem; padding-bottom: 10rem;">
                        <center>
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    <span>{{session('success')}}</span>
                                </div>
                            @else
                                <h4>Kamu belum memiliki akses untuk melihat forecasting.</h4>
                                <h5 style="color: grey; margin-bottom: 30px;">Segera ajukan permintaan untuk melihat forecasting</h5>
                                <form action="{{ route('permission.request') }}" method="post">
                                    @csrf
                                    <button class="btn btn-primary" type="submit">Ajukan</button>
                                </form>
                            @endif
                        </center>
                    </div>
                    @endif
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/forecasting.js" type="text/javascript"></script>
    <script>
        $(".datepicker").datepicker( {
            format: "dd-mm-yyyy",
            viewMode: "months",
            minViewMode: "months"
        });
    </script>
@endsection
