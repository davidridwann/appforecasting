@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Form Supplier
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data supplier</h3> -->
                        <div class="pull-right box-tools">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                <span>{{session('success')}}</span>
                            </div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{session('error')}}</span>
                            </div>
                        @endif
                        @if (@$supplier)
                        <form id="form" action="{{route('supplier.update',$supplier->id)}}" method="post" role="form">
                        @else
                        <form id="form" action="{{route('supplier.store')}}" method="post" role="form">
                        @endif
                        @csrf
                        @if(@$supplier)
                            <input type="hidden" name="_method" value="put" />
                        @endif
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Nama </label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$supplier->nama}}" name="nama" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">No.Telp </label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$supplier->telpon}}" name="telpon" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Alamat </label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$supplier->alamat}}" name="alamat" />
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{route('supplier.index')}}"><button type="button" class="btn btn-default">Batal</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/supplier.js" type="text/javascript"></script>
@endsection
