@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Laporan Persediaan
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @if (Auth::user()->role != 'admin_penjualan')
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data Barang</h3> -->
                        <div class="pull-right box-tools">
                            <form class="form-inline">
                                <a class="btn btn-primary btn-sm" href="{{ route('persediaan.export.all') }}" type="button" style="color:white;">Cetak Semua</a>
                                <a class="btn btn-success btn-sm" id="export-fake" type="button" style="color:white;">Cetak Pilihan</a>
                            </form>
                        </div>
                    </div><!-- /.box-header -->
                    @endif
                    <form id="form" action="{{route('persediaan.export')}}" method="get" role="form">
                        <button class="btn btn-success btn-sm" id="export-real" type="submit" style="display:none;">Cetak Dokumen Real</button>
                        <div class="box-body table-responsive">
                            <table id="table" class="table table-bordered table-hover" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        @if (Auth::user()->role != 'admin_penjualan')
                                        <th>Pilih</th>
                                        @endif
                                        <th>Nama Barang</th>
                                        <th>SKU</th>
                                        <th>Stok</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($datas as $data)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                @if (Auth::user()->role != 'admin_penjualan')
                                                <td class="text-center"><input class="form-control" type="checkbox" value="{{@$data->id}}" name="exports[]" /></td>
                                                @endif
                                                <td>{{$data->nama}}</td>
                                                <td>{{$data->sku}}</td>
                                                <td>{{$data->stock}}</td>
                                            </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/persediaan.js" type="text/javascript"></script>
    <script>
        $('#export-fake').click(function() {
            $('#export-real').click();
        })
    </script>
@endsection
