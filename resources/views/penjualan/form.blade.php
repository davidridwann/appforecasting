@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Form Penjualan
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                <span>{{session('success')}}</span>
                            </div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{session('error')}}</span>
                            </div>
                        @endif
                        @if (@$penjualan)
                        <form id="form" action="{{route('penjualan.update',$penjualan->id)}}" method="post" role="form">
                        @else
                        <form id="form" action="{{route('penjualan.store')}}" method="post" role="form">
                        @endif
                        @csrf
                        @if(@$penjualan)
                            <input type="hidden" name="_method" value="put" />
                        @endif
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Barang </label>
                                <div class="col-xs-9">
                                    <select name="barang_id" id="" class="form-control form-select">
                                        @if(empty(@$penjualan))
                                            <option value="" selected disable>Pilih Barang</option>
                                        @endif
                                        @foreach ($barang as $brg)
                                            <option value="{{ $brg->id }}" {{ @$penjualan->item_id == $brg->id ? 'selected' : '' }}>{{ $brg->nama }} - ( {{ $brg->sku }} )</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Invoice </label>
                                <div class="col-xs-9">
                                    <input class="form-control" required="" value="{{@$penjualan->invoice}}" name="invoice" type="text"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Harga Jual </label>
                                <div class="col-xs-9">
                                    <input class="form-control" required="" value="{{@$penjualan->harga_jual}}" name="harga_jual" type="number"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Jumlah Pembelian </label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="{{@$penjualan->qty}}" name="qty" type="number"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-xs-3">Marketplace </label>
                                <div class="col-xs-9">
                                    <select name="marketplace" id="" class="form-control form-select">
                                        <option value="shopee" {{ @$penjualan->marketplace == 'shopee' ? 'selected' : '' }}>Shopee</option>
                                        <option value="tokopedia" {{ @$penjualan->marketplace == 'tokopedia' ? 'selected' : '' }}>Tokopedia</option>
                                        <option value="lazada" {{ @$penjualan->marketplace == 'lazada' ? 'selected' : '' }}>Lazada</option>
                                        <option value="bukalapak" {{ @$penjualan->marketplace == 'bukalapak' ? 'selected' : '' }}>Bukalapak</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="{{route('penjualan.index')}}"><button type="button" class="btn btn-default">Batal</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/supplier.js" type="text/javascript"></script>
@endsection
