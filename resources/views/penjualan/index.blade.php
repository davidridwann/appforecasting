@extends('app')
@section('css')
<style>
.file-section {
    padding-top: 50px;
    padding-bottom: 50px;
}

.file-drop-area {
  position: relative;
  display: flex;
  align-items: center;
  width: 450px;
  max-width: 100%;
  padding: 25px;
  border: 1px dashed #2ecc71;
  border-radius: 3px;
  transition: 0.2s;
  margin: auto;
  right: 0;
  left: 0;
  &.is-active {
    background-color: rgba(255, 255, 255, 0.05);
  }
}

.fake-btn {
  flex-shrink: 0;
  background-color: #2ecc71;
  border: 1px solid #2ecc71;
  border-radius: 3px;
  padding: 8px 15px;
  margin-right: 10px;
  font-size: 12px;
  text-transform: uppercase;
  color: white;
  font-weight: bold;
}

#type-import {
  position: absolute;
  display: flex;
  align-items: center;
  width: 200px;
  max-width: 100%;
  border: 1px solid #2ecc71;
  border-radius: 3px;
  padding: 8px 15px;
  margin-right: 10px;
  font-size: 12px;
  text-transform: uppercase;
  color: #2ecc71;
  font-weight: bold;
  margin: auto;
  right: 58%;
  left: 0;
}

#type-import:focus {
    border-color: unset !important;
}

.file-msg {
  font-size: small;
  font-weight: 300;
  line-height: 1.4;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.file-input {
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  cursor: pointer;
  opacity: 0;
  &:focus {
    outline: none;
  }
}

.d-none {
    display:none !important;
}
</style>
@endsection
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Laporan Penjualan
          <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data Barang</h3> -->
                        <div class="pull-right box-tools">
                            @if (count($datas) == 0)
                                <form class="form-inline">
                                    <a class="btn btn-primary btn-sm" style="color:white;" href="{{ route('penjualan.create') }}">Buat</a>
                                    <a class="btn btn-success btn-sm" id="fake-btn"  style="color:white;">Import</a>
                                </form>
                            @else
                                <form class="form-inline">
                                    <a class="btn btn-warning btn-sm d-none" id="cancel-to-import" style="color:white;">Batal</a>
                                    <a class="btn btn-success btn-sm d-none" id="fake-btn" style="color:white;">Submit</a>
                                    <a class="btn btn-primary btn-sm" style="color:white;" href="{{ route('penjualan.create') }}">Buat</a>
                                    <a class="btn btn-success btn-sm" id="change-to-import" style="color:white;">Import</a>
                                </form>
                            @endif
                        </div>
                    </div><!-- /.box-header -->

                    <!-- Bila data sudah ada maka akan mengunakan form ini -->
                    @if (count($datas) != 0)
                        <div class="box-body d-none" id="section-import">
                            <form id="form" action="{{route('laporan_penjualan.import')}}" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="file-section">
                                    <div>
                                        <select name="type" id="type-import">
                                            <option value="shopee">Shopee</option>
                                            <option value="lazada">Lazada</option>
                                            <option value="bukalapak">Bukalapak</option>
                                            <option value="tokopedia">Tokopedia</option>
                                        </select>
                                    </div>
                                    <div class="file-drop-area">
                                        <span class="fake-btn">Choose files</span>
                                        <span class="file-msg">or drag and drop files here</span>
                                        <input class="file-input" type="file" name="file">
                                    </div>
                                    @if ($errors->has('file'))
                                        <span role="alert" style="color:#e74c3c;">
                                            <strong>* {{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button class="btn btn-success btn-sm" type="submit" id="real-btn" style="display:none;">Import</button>
                            </form>
                        </div>
                    @endif

                    <div class="box-body table-responsive" id="section-table">
                        @if (count($datas) != 0)
                            <table id="table" class="table table-bordered table-hover" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>SKU</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah Pembelian</th>
                                        <th>Stok Sebelum</th>
                                        <th>Stok Sesudah</th>
                                        <th>Marketplace</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($datas as $data)
                                        @if ($data->type == 'import')
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$data->barang->sku}}</td>
                                                <td>{{$data->barang->nama}}</td>
                                                <td>{{$data->qty}}</td>
                                                <td>{{$data->stock_sebelum}}</td>
                                                <td>{{$data->stock_sesudah}}</td>
                                                <td style="text-transform: capitalize;">{{$data->marketplace}}</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td><a href="{{ route('penjualan.edit', $data->id) }}" style="color: black;">{{$i++}}</a></td>
                                                <td><a href="{{ route('penjualan.edit', $data->id) }}" style="color: black;">{{$data->barang->sku}}</a></td>
                                                <td><a href="{{ route('penjualan.edit', $data->id) }}" style="color: orange;">{{$data->barang->nama}}</a></td>
                                                <td><a href="{{ route('penjualan.edit', $data->id) }}" style="color: black;">{{$data->qty}}</a></td>
                                                <td><a href="{{ route('penjualan.edit', $data->id) }}" style="color: black;">{{$data->stock_sebelum}}</a></td>
                                                <td><a href="{{ route('penjualan.edit', $data->id) }}" style="color: black;">{{$data->stock_sesudah}}</a></td>
                                                <td style="text-transform: capitalize;"><a href="{{ route('penjualan.edit', $data->id) }}" style="color: black;">{{$data->marketplace}}</a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <form id="form" action="{{route('laporan_penjualan.import')}}" method="post" role="form" enctype="multipart/form-data">
                                @csrf
                                <div class="file-section">
                                    <div>
                                        <select name="type" id="type-import">
                                            <option value="shopee">Shopee</option>
                                            <option value="lazada">Lazada</option>
                                            <option value="bukalapak">Bukalapak</option>
                                            <option value="tokopedia">Tokopedia</option>
                                        </select>
                                    </div>
                                    <div class="file-drop-area">
                                        <span class="fake-btn">Choose files</span>
                                        <span class="file-msg">or drag and drop files here</span>
                                        <input class="file-input" type="file" name="file">
                                    </div>
                                    @if ($errors->has('file'))
                                        <span role="alert" style="color:#e74c3c;">
                                            <strong>* {{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button class="btn btn-success btn-sm" type="submit" id="real-btn" style="display:none;">Import</button>
                            </form>
                        @endif
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/penjualan.js" type="text/javascript"></script>
    <script>
        var $fileInput = $('.file-input');
        var $droparea = $('.file-drop-area');

        $fileInput.on('dragenter focus click', function() {
            $droparea.addClass('is-active');
        });

        $fileInput.on('dragleave blur drop', function() {
            $droparea.removeClass('is-active');
        });

        $fileInput.on('change', function() {
            var filesCount = $(this)[0].files.length;
            var $textContainer = $(this).prev();

            if (filesCount === 1) {
                var fileName = $(this).val().split('\\').pop();
                $textContainer.text(fileName);
            } else {
                $textContainer.text(filesCount + ' files selected');
            }
        });

        $('#fake-btn').click(function() {
            $('#real-btn').click();
        })

        $('#change-to-import').click(function() {
            $('#section-table').addClass('d-none');
            $('#change-to-import').addClass('d-none');
            $('#section-import').removeClass('d-none');
            $('#cancel-to-import').removeClass('d-none');
            $('#fake-btn').removeClass('d-none');
        })

        $('#cancel-to-import').click(function() {
            $('#section-import').addClass('d-none');
            $('#fake-btn').addClass('d-none');
            $('#cancel-to-import').addClass('d-none');
            $('#section-table').removeClass('d-none');
            $('#change-to-import').removeClass('d-none');
        })
    </script>
@endsection
