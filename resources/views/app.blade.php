<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>APLIKASI FORECASTING</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo url('assets'); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo url('assets'); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo url('assets'); ?>/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo url('assets'); ?>/vendor/DataTables/datatables.min.css" rel="stylesheet" media="all">
        <link href="<?php echo url('assets'); ?>/vendor/toastr/build/toastr.min.css" rel="stylesheet" media="all">
        <link href="<?php echo url('assets'); ?>/vendor/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="all">
        <link href="<?php echo url('assets'); ?>/vendor/datepicker/bootstrap-datepicker.min.css" rel="stylesheet" media="all">
        <link href="<?php echo url('assets'); ?>/vendor/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet" media="all">
        <!-- Theme style -->
        <link href="<?php echo url('assets'); ?>/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('css')
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a style="text-align:left;font-size:14px;line-height:16px;" href="<?php echo url('/'); ?>" class="logo form-inline">
                <table style="width:100%;height:100%;">
                    <tr>
                        <td>
                            <img src="{{ asset('assets/img/logo.png') }}" width="170px"/>
                        </td>
                    </tr>
                </table>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo auth()->user()->name; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo url('assets') ?>/img/avatar3.png" class="img-circle" />
                                    <p>
                                        <?php echo auth()->user()->name; ?>
                                        <!-- <small>Member since Nov. 2012</small> -->
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?php echo url('/logout'); ?>" class="btn btn-default btn-flat">Keluar</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <!-- <div class="user-panel">
                        <div class="pull-left image">
                            <?php if (auth()->user()->photo): ?>
                                <img src="<?php echo url('uploads').'/'.auth()->user()->id.'/'.auth()->user()->photo ?>" class="img-circle" />
                            <?php else: ?>
                                <img src="<?php echo url('assets') ?>/img/avatar3.png" class="img-circle" />
                            <?php endif ?>
                        </div>
                        <div class="pull-left info">
                            <p>Halo, <?php echo auth()->user()->name; ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Aktif</a>
                        </div>
                    </div> -->
                    <!-- search form -->
                    <!-- <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form> -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="menuhome">
                            <a href="<?php echo url('/'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Home</span>
                            </a>
                        </li>
                        @if (Auth::user()->role != 'admin_penjualan' && Auth::user()->role != 'owner')
                        <li class="menubarang">
                            <a href="<?php echo url('/barang'); ?>">
                                <i class="fa fa-table"></i> <span>Data Barang</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role != 'admin_penjualan' && Auth::user()->role != 'owner')
                        <li class="menutipe">
                            <a href="<?php echo url('/tipe'); ?>">
                                <i class="fa fa-th"></i> <span>Data Tipe</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role == 'owner')
                        <li class="menuuser">
                            <a href="<?php echo url('/user'); ?>">
                                <i class="fa fa-user"></i> <span>Data User</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role != 'admin_penjualan' && Auth::user()->role != 'owner')
                        <li class="menusupplier">
                            <a href="<?php echo url('/supplier'); ?>">
                                <i class="fa fa-truck"></i> <span>Data Supplier</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role != 'owner')
                        <li class="menupersediaan">
                            <a href="<?php echo url('/laporan_persediaan'); ?>">
                                <i class="fa fa-check-square-o"></i> <span>Laporan Persediaan</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role != 'admin_gudang')
                        <li class="menupenjualan">
                            <a href="<?php echo url('/laporan_penjualan'); ?>">
                                <i class="fa fa-money"></i> <span>Laporan Penjualan</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role != 'admin_penjualan' && Auth::user()->role != 'owner')
                        <li class="menupembelian">
                            <a href="<?php echo url('/laporan_pembelian'); ?>">
                                <i class="fa fa-shopping-cart"></i> <span>Laporan Pembelian</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role != 'owner' && Auth::user()->role != 'admin_gudang')
                        <li class="menuforecasting">
                            <a href="<?php echo url('/forecasting'); ?>">
                                <i class="fa fa-bolt"></i> <span>Forecasting</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::user()->role != 'admin_penjualan' && Auth::user()->role != 'admin_gudang')
                        <li class="menupermission">
                            <a href="<?php echo url('/permission'); ?>">
                                <i class="fa fa-bolt"></i> <span>Permintaan Akses Forecasting</span>
                            </a>
                        </li>
                        @endif
                        <!-- <li class="treeview menudata">
                            <a href="#">
                                <i class="fa fa-table"></i> <span>Data</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="menuuser"><a href="<?php echo url('/'); ?>user.html"><i class="fa fa-angle-double-right"></i> User</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            @yield('content')
        </div><!-- ./wrapper -->


        <script src="<?php echo url('assets'); ?>/jquery.js"></script>
        <script src="<?php echo url('assets'); ?>/moment.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo url('assets'); ?>/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo url('assets'); ?>/vendor/DataTables/datatables.min.js"></script>
        <script src="<?php echo url('assets'); ?>/vendor/toastr/build/toastr.min.js"></script>
        <script src="<?php echo url('assets'); ?>/vendor/datetimepicker/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo url('assets'); ?>/vendor/datepicker/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo url('assets'); ?>/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
        <script src="<?php echo url('assets'); ?>/js/app.js" type="text/javascript"></script>
        <script src="<?php echo url('assets'); ?>/js/chart.js" type="text/javascript"></script>
        <script src="<?php echo url('assets'); ?>/js/datalabel.js" type="text/javascript"></script>
        <script type="text/javascript">
            var baseUrl = '<?php echo url('/'); ?>';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('keypress','.only-numeric',function(e){
                var keyCode = e.which ? e.which : e.keyCode
                if (!(keyCode >= 48 && keyCode <= 57)) {
                    return false;
                }
            });
            window.formatAsCurrency = function (value, dec) {
                dec = dec || 0;
                if (value === null) {
                    return 0;
                }
                value = parseInt(value) || 0;
                return '' + value.toFixed(dec).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
            }
        </script>
        @yield('js')

    </body>

</html>
<!-- end document-->
