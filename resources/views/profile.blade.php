@extends('app')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profil
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <!-- <h3 class="box-title">Data User</h3> -->
                        <div class="pull-right box-tools">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <form class="form-horizontal" id="formprofile">
                            <input type="hidden" required="" name="id" />
                            <div class="modal-body">
                              <!-- <div class="form-group">
                                <div class="col-xs-2">
                                    <?php if (auth()->user()->photo): ?>
                                        <img id="imgreview" src="<?php echo url('uploads').'/'.auth()->user()->id.'/'.auth()->user()->photo ?>" class="img-responsive" />
                                    <?php else: ?>
                                        <img id="imgreview" src="<?php echo url('assets') ?>/img/avatar2.png" class="img-responsive" />
                                    <?php endif ?>
                                </div>
                                <div class="col-xs-9">
                                Max 100kb <input class="form-control" id="photo" name="photo" type="file" accept="image/*" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-xs-2">Nama Lengkap :</label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="<?php echo auth()->user()->fullname; ?>" name="fullname" />
                                </div>
                              </div> -->
                              <div class="form-group">
                                <label class="control-label col-xs-2">Input Password Lama :</label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="" name="old_password" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-xs-2">Input Password Baru :</label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="" name="new_password" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-xs-2">Konfirmasi Password Baru :</label>
                                <div class="col-xs-9">
                                <input class="form-control" required="" value="" name="confirm_new_password" />
                                </div>
                              </div>
                            </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="btn-save">Simpan</button>
                          </div>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->
</aside>
@endsection
@section('js')
    <script src="<?php echo url('assets'); ?>/pages/profile.js" type="text/javascript"></script>
@endsection
