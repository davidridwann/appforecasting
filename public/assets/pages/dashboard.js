$('.menudashboard').addClass("active");

var table = $("#table").DataTable({
    filter : true,
    sortable: false,
    info: true,
    paging: true,
    processing: true,
    serverSide: true,
    ordering : true,
    order : [[0,'desc']],
    ajax: {
        url: baseUrl + "history/today.html",
        data : function(data){
            return {
                draw: data.draw,
                start: data.start,
                length: data.length,
                search: data.search["value"],
                order:data.order
            };
        },
    },
    columns: [
        { data: "jam" },
        { data: "periode" },
        { data: "batch" },
        { data: "beban" }
    ],
    columnDefs: [
        // {
        //     render: function(data,type,row,index){
        //         var info = table.page.info();
        //         return index.row+info.start+1;
        //     },
        //     targets : [0]
        // },
        {
            render: function(data,type,row,index){
                if (data)
                    return moment(data).format("DD-MM-YYYY HH:mm");
                return '';
            },
            targets : [0]
        },
        // {
        //     render: function(data,type,row,index){
        //         if (data == 'Lunas')
        //             return '<span class="label label-success" style="font-weight:bold;">Lunas</span>'
        //         return '<span class="label label-danger" style="font-weight:bold;">Belum Lunas</span>'
        //     },
        //     targets : [5]
        // },
        {
            render: function(data,type,row,index){
                if(data)
                    return formatAsCurrency(data);
                return 0;
            },
            targets : [3]
        }
    ],
    drawCallback: function(e,response){
        
    }
});

function summary(){
    $.get(baseUrl + "history/summary.html", function(resp) {
        // alert( "success" );
        $("#batch").html(resp.batch);
        $("#beban").html(resp.beban);
        $("#totalhariini").html(resp.totalhariini);
        $("#totalizer").html(resp.totalizer);
    })
    .done(function() {
        // alert( "second success" );
    })
    .fail(function() {
        // alert( "error" );
    })
    .always(function() {
        // alert( "finished" );
    });
}

function reload(){
    summary();
    table.ajax.reload();
    setTimeout(function() {
        reload();
    }, 5000);
}

reload();