$(document).ready(function() {

    $("#formprofile").submit(function(event) {
        event.preventDefault();
        var btn = $("#btn-save");
        btn.text('Menyimpan Data...');
        btn.attr('disabled', true);

        var form = $('#formprofile')[0]; 
        var formData = new FormData(form);
        $.ajax({
            url: baseUrl + "/profile",
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
        })
        .done(function(resp) {
            toastr[resp.status](resp.message);
            if(resp.success)
        	    location.reload();
        })
        .fail(function() {
     		toastr['warning']('Tidak dapat terhubung ke server !!!');
        })
        .always(function() {
            btn.text('Simpan');
            btn.attr('disabled', false);
        });
        
        return false;
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#imgreview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#photo").change(function(){
        readURL(this);
    });
} );