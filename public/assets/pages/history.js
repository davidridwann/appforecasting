$(document).ready(function() {
	$('.menuhistory').addClass("active");
	$('.date').datepicker({	
    	format: "dd-mm-yyyy",
	    autoclose:true
    });

	var table = $("#table").DataTable({
		filter : true,
		sortable: false,
		info: true,
		paging: true,
		processing: true,
		serverSide: true,
		ordering : true,
		order : [[0,'desc']],
		ajax: {
			url: baseUrl + "history/data.html",
			data : function(data){
				return {
					draw: data.draw,
					start: data.start,
					length: data.length,
					search: data.search["value"],
                    order:data.order,
					startdate : $('input[name="filtertanggal"]').val(),
					enddate : $('input[name="filtertanggal1"]').val()
				};
			},
		},
		columns: [
			{ data: "jam" },
			{ data: "periode" },
			{ data: "batch" },
			{ data: "beban" }
		],
		columnDefs: [
		 	// {
            //     render: function(data,type,row,index){
            //         var info = table.page.info();
            //         return index.row+info.start+1;
            //     },
            //     targets : [0]
            // },
            {
                render: function(data,type,row,index){
                    if (data)
                        return moment(data).format("DD-MM-YYYY HH:mm");
                    return '';
                },
                targets : [0]
            },
            // {
            //     render: function(data,type,row,index){
            //         if (data == 'Lunas')
            //             return '<span class="label label-success" style="font-weight:bold;">Lunas</span>'
            //         return '<span class="label label-danger" style="font-weight:bold;">Belum Lunas</span>'
            //     },
            //     targets : [5]
            // },
            {
                render: function(data,type,row,index){
					if(data)
						return formatAsCurrency(data);
					return 0;
                },
                targets : [3]
            }
		],
		drawCallback: function(e,response){
			
		}
	});

	$('input[name="filtertanggal"]').change(function(event) {
		// var val = $(this).val();
		// if (!val) {
		// 	$(this).val(moment(new Date()).format("DD-MM-YYYY"));
		// }
		table.ajax.reload();
	});

	$('input[name="filtertanggal1"]').change(function(event) {
		table.ajax.reload();
	});

	$("#btn-excel").click(function(e){
		event.preventDefault();
		var btn = $(this);
		btn.html('<i>Loading...</i>');
		$.ajax({
			url: baseUrl + "/history/excel.html",
			type: 'POST',
			dataType: 'json',
			data: {
				startdate : $('input[name="filtertanggal"]').val(),
				enddate : $('input[name="filtertanggal1"]').val()
			},
		})
		.done(function(resp) {
			if(resp.success)
				location.replace(resp.data);
			else
				toastr[resp.status](resp.message);
		})
		.fail(function() {
			 toastr['warning']('Tidak dapat terhubung ke server !!!');
		})
		.always(function() {
			btn.html('Excel');
			$(".actionbtn").attr('disabled', false);
		});
		
		return false;
	});

	$("#btn-csv").click(function(e){
		event.preventDefault();
		var btn = $(this);
		btn.html('<i>Loading...</i>');
		$.ajax({
			url: baseUrl + "/history/csv.html",
			type: 'POST',
			dataType: 'json',
			data: {
				startdate : $('input[name="filtertanggal"]').val(),
				enddate : $('input[name="filtertanggal1"]').val()
			},
		})
		.done(function(resp) {
			if(resp.success)
				window.open(resp.data,"_blank");
			else
				toastr[resp.status](resp.message);
		})
		.fail(function() {
			 toastr['warning']('Tidak dapat terhubung ke server !!!');
		})
		.always(function() {
			btn.html('Csv');
			$(".actionbtn").attr('disabled', false);
		});
		
		return false;
	});
} );