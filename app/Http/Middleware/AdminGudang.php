<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminGudang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() &&  Auth::user()->role != 'admin_penjualan') {
            return $next($request);
        }

        return redirect('/')->with('error','Kamu tidak memiliki akses pada halaman ini');
    }
}
