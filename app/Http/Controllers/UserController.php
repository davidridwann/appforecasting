<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('owner');
    }

    protected $customMessages = [
        'required' => ':attribute Harus Diisi',
        'unique' => ':attribute Sudah Ada Didatabase',
        'max' => ':attribute Max :max Digit',
        'min' => ':attribute Minimal :min',
    ];
    protected $customeAttr = [
        'username' => 'Username',
        'name' => 'Name',
        'password' => 'Password',
        'role' => 'Role',
        'is_active' => 'Active',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['datas'] = User::get();
        return view('user.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return view('user.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'username' => 'required|max:255|unique:user,username,0,id',
            'password' => 'required|max:255',
            'role' => 'required|max:255',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('name','role','username');
        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        }
        try {
            $created = User::create($data);
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error,'.$e->getMessage());
        }
        return redirect()->route('user.index')->withSuccess('data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['user'] = User::find($id);
        return view('user.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'username' => 'required|max:255|unique:user,username,'.$id.',id',
            'name' => 'required|max:255',
            'role' => 'required|max:255',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('name','role','username');
        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        }
        $data['updated_at'] = date('Y-m-d H:i:s');
        try {
            $row = User::find($id);
            $row->fill($data);
            $saved = $row->save();
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error');
        }
        return redirect()->route('user.index')->withSuccess('data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('user.index')->withSuccess('data berhasil di hapus');
    }
}
