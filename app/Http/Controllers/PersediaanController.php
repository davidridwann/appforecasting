<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use App\Exports\PersediaanExport;
use Maatwebsite\Excel\Facades\Excel;

class PersediaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['datas'] = Barang::get();
        return view('persediaan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Export selected or all resource from storage.
     *
     * @param Request $request
     * @return excel
     */
    public function exportExcel(Request $request)
    {
        $pointers = $request->input('exports');
        $items = Barang::whereIn('id', array_values($pointers))->get();

        $arrayItem = [];
        $keys = [];
        foreach($items as $index => $item) {
            $data = [
                'No.' => $index + 1,
                'SKU' => $item->sku,
                'Nama Barang' => $item->nama,
                'Harga Jual' => $item->harga_jual,
                'Harga Beli' => $item->harga_beli,
                'Stok Barang' => $item->stock
            ];

            array_push($arrayItem, $data);

            $keys = array_keys($data);
        }

        return Excel::download(new PersediaanExport($arrayItem, $keys), 'stock.xlsx');
    }

    public function exportExcelAll()
    {
        $items = Barang::get();

        $arrayItem = [];
        $keys = [];
        foreach($items as $index => $item) {
            $data = [
                'No.' => $index + 1,
                'SKU' => $item->sku,
                'Nama Barang' => $item->nama,
                'Harga Jual' => $item->harga_jual,
                'Harga Beli' => $item->harga_beli,
                'Stok Barang' => $item->stock
            ];

            array_push($arrayItem, $data);

            $keys = array_keys($data);
        }

        return Excel::download(new PersediaanExport($arrayItem, $keys), 'stock.xlsx');
    }
}
