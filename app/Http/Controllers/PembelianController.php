<?php

namespace App\Http\Controllers;

use App\Models\Pembelian;
use App\Models\Supplier;
use App\Models\Barang;
use App\Exports\PembelianExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PembelianController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin_gudang');
    }

    protected $customMessages = [
        'required' => ':attribute Harus Diisi',
        'unique' => ':attribute Sudah Ada Didatabase',
        'numeric' => ':attribute Harus Angka',
        'max' => ':attribute Max :max Digit',
        'min' => ':attribute Minimal :min',
    ];
    protected $customeAttr = [
        'barang_id' => 'Barang',
        'harga_beli' => 'Harga Beli',
        'supplier_id' => 'Supplier',
        'qty' => 'Kuantiti'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['datas'] = Pembelian::get();

        return view('pembelian.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['items'] = Barang::get();
        $data['suppliers'] = Supplier::get();

        return view('pembelian.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'barang_id' => 'required',
            'harga_beli' => 'required|numeric',
            'supplier_id' => 'required',
            'qty' => 'required|numeric',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('barang_id','harga_beli','supplier_id','qty');
        try {
            $created = Pembelian::create($data);

            if ($created) {
                $barang = Barang::find($request->barang_id);
                $newStock = $barang->stock + (int)$request->qty;
                $barang->update([
                    'stock' => $newStock
                ]);
            }
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error,'.$e->getMessage());
        }
        return redirect()->route('laporan_pembelian.index')->withSuccess('data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['items'] = Barang::get();
        $data['suppliers'] = Supplier::get();
        $data['pembelian'] = Pembelian::find($id);

        return view('pembelian.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'barang_id' => 'required',
            'harga_beli' => 'required|numeric',
            'supplier_id' => 'required',
            'qty' => 'required|numeric',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('barang_id','harga_beli','supplier_id','qty');
        $data['updated_at'] = date('Y-m-d H:i:s');
        try {
            $row = Pembelian::find($id);
            $row->fill($data);
            $saved = $row->save();
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error');
        }
        return redirect()->route('laporan_pembelian.index')->withSuccess('data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pembelian = Pembelian::find($id);
        $deleted = $pembelian->delete();

        if ($deleted) {
            $barang = Barang::find($pembelian->barang_id);
            $newStock = $barang->stock - (int)$pembelian->qty;
            $barang->update([
                'stock' => $newStock
            ]);
        }
        return redirect()->route('laporan_pembelian.index')->withSuccess('data berhasil di hapus');
    }

    public function exportExcel(Request $request)
    {
        $pointers = $request->input('exports');
        $items = Pembelian::whereIn('id', array_values($pointers))->get();

        $arrayItem = [];
        $keys = [];
        foreach($items as $index => $item) {
            $data = [
                'No.' => $index + 1,
                'SKU' => $item->barang->sku,
                'Nama Barang' => $item->barang->nama,
                'Harga Beli' => $item->harga_beli,
                'Kuantiti Pembelian' => $item->qty,
                'Stok' => $item->barang->stock
            ];

            array_push($arrayItem, $data);

            $keys = array_keys($data);
        }

        return Excel::download(new PembelianExport($arrayItem, $keys), 'pembelian.xlsx');
    }

    public function exportExcelAll()
    {
        $items = Pembelian::get();

        $arrayItem = [];
        $keys = [];
        foreach($items as $index => $item) {
            $data = [
                'No.' => $index + 1,
                'SKU' => $item->barang->sku,
                'Nama Barang' => $item->barang->nama,
                'Harga Beli' => $item->harga_beli,
                'Kuantiti Pembelian' => $item->qty,
                'Stok' => $item->barang->stock
            ];

            array_push($arrayItem, $data);

            $keys = array_keys($data);
        }

        return Excel::download(new PembelianExport($arrayItem, $keys), 'pembelian.xlsx');
    }
}
