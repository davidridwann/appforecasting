<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use Illuminate\Support\Facades\Validator;

class SupplierController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin_gudang');
    }


    protected $customMessages = [
        'required' => ':attribute Harus Diisi',
        'unique' => ':attribute Sudah Ada Didatabase',
        'max' => ':attribute Max :max Digit',
        'min' => ':attribute Minimal :min',
    ];
    protected $customeAttr = [
        'nama' => 'nama',
        'telpon' => 'telpon',
        'alamat' => 'alamat',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['datas'] = Supplier::get();
        return view('supplier.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return view('supplier.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'telpon' => 'required|max:255',
            'alamat' => 'required|max:255',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('nama','telpon','alamat');
        try {
            $created = Supplier::create($data);
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error,'.$e->getMessage());
        }
        return redirect()->route('supplier.index')->withSuccess('data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['supplier'] = Supplier::find($id);
        return view('supplier.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'telpon' => 'required|max:255',
            'alamat' => 'required|max:255',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('nama','telpon','alamat');
        $data['updated_at'] = date('Y-m-d H:i:s');
        try {
            $row = Supplier::find($id);
            $row->fill($data);
            $saved = $row->save();
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error');
        }
        return redirect()->route('supplier.index')->withSuccess('data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Supplier = Supplier::find($id);
        $Supplier->delete();
        return redirect()->route('supplier.index')->withSuccess('data berhasil di hapus');
    }
}
