<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\Penjualan;
use Hash;

class HomeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        setlocale(LC_ALL, 'id_ID');

        $object = [];
        $values = [];

        if (!empty($request->type)) {
            if ($request->type == 'month') {
                $startMonth = Carbon::now()->startOfYear()->format('M');
                $endMonth = Carbon::now()->endOfYear()->format('M');
                $monthRange = CarbonPeriod::create($startMonth, '1 month', $endMonth);
                foreach ($monthRange as $month){
                   $object[] = Carbon::parse($month)->format('F');
                   $startMonth = Carbon::parse($month)->startOfMonth();
                   $endMonth = Carbon::parse($month)->endOfMonth();

                   $values[] = Penjualan::whereBetween('created_at', [$startMonth, $endMonth])->count();
                }
            } else {
                $weekMap = [
                    0 => 'Minggu',
                    1 => 'Senin',
                    2 => 'Selasa',
                    3 => 'Rabu',
                    4 => 'Kamis',
                    5 => 'Jumat',
                    6 => 'Sabtu',
                ];

                $valueMap = [
                    0 => 0,
                    1 => 0,
                    2 => 0,
                    3 => 0,
                    4 => 0,
                    5 => 0,
                    6 => 0,
                ];

                $sales = Penjualan::get();

                foreach($sales as $sale) {
                    $dayOfTheWeek = Carbon::parse($sale->created_at)->dayOfWeek;

                    if ($valueMap[$dayOfTheWeek] != 0) {
                        $newVal = $valueMap[$dayOfTheWeek] + 1;
                        $valueMap[$dayOfTheWeek] = $newVal;
                    } else {
                        $valueMap[$dayOfTheWeek] = 1;
                    }
                }


                $object = $weekMap;
                $values = $valueMap;
            }
        } else {
            $weekMap = [
                0 => 'Minggu',
                1 => 'Senin',
                2 => 'Selasa',
                3 => 'Rabu',
                4 => 'Kamis',
                5 => 'Jumat',
                6 => 'Sabtu',
            ];

            $valueMap = [
                0 => 0,
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0,
                6 => 0,
            ];

            $sales = Penjualan::get();

            foreach($sales as $sale) {
                $dayOfTheWeek = Carbon::parse($sale->created_at)->dayOfWeek;

                if ($valueMap[$dayOfTheWeek] != 0) {
                    $newVal = $valueMap[$dayOfTheWeek] + 1;
                    $valueMap[$dayOfTheWeek] = $newVal;
                } else {
                    $valueMap[$dayOfTheWeek] = 1;
                }
            }

            $object = $weekMap;
            $values = $valueMap;
        }

        return view('home')->with([
            'object' => $object,
            'values' => $values
        ]);
    }

    public function indexProfile()
    {
        return view('profile');
    }

    public function storeProfile(Request $request)
    {
        $user = User::selectRaw("user.*")->where('id',auth()->user()->id)->first();
        $fullname = $request->fullname;
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $confirm_password = $request->confirm_new_password;

        if(!Hash::check($old_password, $user->password)){
            return ['status'=>'error','success'=>false,'message'=>'Password Lama tidak sesuai'];
        }
        if($new_password != $confirm_password){
            return ['status'=>'error','success'=>false,'message'=>'Password Baru dan Konfirmasi tidak sesuai'];
        }
        // $data['fullname'] = $fullname;
        $user->password = bcrypt($new_password);
        $saved = $user->save();
        if ($saved) {
            $resp = ['status'=>'success','success'=>true,'message'=>'Profil Berhasil di perbarui'];
        }else{
            $resp = ['status'=>'error','success'=>false,'message'=>'Profil gagal di perbarui'];
        }
        return $resp;
    }
}
