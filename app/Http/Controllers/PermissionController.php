<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;
use Auth;

class PermissionController extends Controller
{
    public function index()
    {
        $data = [];
        $data['datas'] = Permission::orderBy('created_at', 'DESC')->get();

        return view('permission.index', $data);
    }

    public function requestPermission()
    {
        $data = [];
        $data['user_id'] = Auth::user()->id;
        $data['status'] = 'pending';

        Permission::create($data);


        return redirect()->back()->withSuccess('Berhasil mengajukan permintaan');
    }

    public function acceptPermission($id)
    {
        Permission::where('id', $id)->update([
            'status' => 'accepted'
        ]);

        return redirect()->back()->withSuccess('Berhasil menerima permintaan');
    }

    public function declinePermission($id)
    {
        Permission::where('id', $id)->update([
            'status' => 'declined'
        ]);

        return redirect()->back()->withSuccess('Berhasil menolak permintaan');
    }
}
