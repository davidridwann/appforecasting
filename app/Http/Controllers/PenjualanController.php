<?php

namespace App\Http\Controllers;

use App\Models\Penjualan;
use App\Models\Barang;
use App\Imports\PenjualanShopeeImport;
use App\Imports\PenjualanTokopediaImport;
use App\Imports\PenjualanBukalapakImport;
use App\Imports\PenjualanLazadaImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PenjualanController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin_penjualan');
    }


    protected $customMessages = [
        'required' => ':attribute Harus Diisi',
        'mimes' => ':attribute Harus Format .xls atau .xlsx',
        'required' => ':attribute Harus Diisi',
        'max' => ':attribute Max :max Digit',
        'min' => ':attribute Minimal :min',
        'mimes' => ':attribute Harus Format .xls atau .xlsx',
    ];
    protected $customeAttr = [
        'file' => 'File',
        'barang_id' => 'Barang',
        'harga_jual' => 'Harga Jual',
        'qty' => 'Jumlah Penjualan',
        'marketplace' => 'Marketplace'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['datas'] = Penjualan::orderBy('created_at', 'DESC')->get();
        return view('penjualan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['barang'] = Barang::get();

        return view('penjualan.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'barang_id' => 'required|max:255',
            'harga_jual' => 'required|min:0',
            'qty' => 'required|min:0',
            'marketplace' => 'required'
        ],$this->customMessages,$this->customeAttr);

        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }

        $data = $request->only('barang_id','invoice','harga_jual','qty','marketplace');

        try {
            $barang = Barang::where('id', $data['barang_id'])->first();
            $newStock = $barang->stock - (int)$data['qty'];

            $data['stock_sebelum'] = $barang->stock;
            $data['stock_sesudah'] = $newStock;
            $data['type'] = 'manual';

            $created = Penjualan::create($data);
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error,'.$e->getMessage());
        }
        return redirect()->route('penjualan.index')->withSuccess('data berhasil disimpan');
    }

    /**
     * Show the form for editing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['barang'] = Barang::get();
        $data['penjualan'] = Penjualan::where('id', $id)->first();

        return view('penjualan.form',$data);
    }

    /**
     * Update a specific resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'barang_id' => 'required|max:255',
            'harga_jual' => 'required|min:0',
            'qty' => 'required|min:0',
            'marketplace' => 'required'
        ],$this->customMessages,$this->customeAttr);

        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }

        $data = $request->only('barang_id','invoice','harga_jual','qty','marketplace');

        try {
            $barang = Barang::where('id', $data['barang_id'])->first();
            $newStock = $barang->stock - (int)$data['qty'];

            $data['stock_sebelum'] = $barang->stock;
            $data['stock_sesudah'] = $newStock;
            $data['type'] = $manual;

            $created = Penjualan::where('id', $id)->update($data);
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error,'.$e->getMessage());
        }
        return redirect()->route('penjualan.index')->withSuccess('data berhasil dirubah');
    }

    /**
     * Store a newly created resource via import excel in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        if ($request->type == 'bukalapak') {
            $this->validate($request, [
                'file' => 'required'
            ], $this->customMessages,$this->customeAttr);
        } else if ($request->type == 'lazada') {
            $this->validate($request, [
                'file' => 'required'
            ], $this->customMessages,$this->customeAttr);
        } else {
            $this->validate($request, [
                'file' => 'required|mimes:xls,xlsx'
            ], $this->customMessages,$this->customeAttr);
        }

		$file = $request->file('file');

		$nama_file = rand().$file->getClientOriginalName();

		$file->move('penjualan',$nama_file);

		if ($request->type == 'shopee') {
            Excel::import(new PenjualanShopeeImport, public_path('/penjualan/'.$nama_file));
        } else if ($request->type == 'tokopedia') {
            Excel::import(new PenjualanTokopediaImport, public_path('/penjualan/'.$nama_file));
        } else if ($request->type == 'bukalapak') {
            Excel::import(new PenjualanBukalapakImport, public_path('/penjualan/'.$nama_file));
        } else if ($request->type == 'lazada') {
            Excel::import(new PenjualanLazadaImport, public_path('/penjualan/'.$nama_file));
        }

		return redirect()->back()->withSuccess('Berhasil import data excel');
    }
}
