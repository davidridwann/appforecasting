<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Penjualan;
use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class ForecastingController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin_penjualan');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $datas = [];
        $diff = 0;
        $status = false;
        $monthValidation = false;
        $getTotalForecasting = 0;
        $forecastingPermission = false;
        $diffForecasting = 0;

        $getMSE1 = 0;
        $getMSE2 = 0;
        $getRMSE = 0;

        $checkPermission = Permission::where('user_id', Auth::user()->id)
            ->whereDate('created_at', Carbon::now())
            ->where('status', 'accepted')
            ->first();

        if ($checkPermission) {
            $forecastingPermission = true;
        }

        if (!empty($request->dari)) {
            $datas = Barang::get();

            $status = true;

            $dari = Carbon::parse($request->dari)->firstOfMonth();
            $sampai = Carbon::parse($request->sampai)->endOfMonth();


            $periodDari = $dari;
            $periodSampai = $sampai;

            $reqForecasting = $request->forecasting;

            $checkTglPenjualanTerakhir = Penjualan::orderBy('created_at', 'desc')->first();

            if (Carbon::parse($checkTglPenjualanTerakhir->created_at)->format('Y-m') > Carbon::parse($sampai)->format('Y-m')) {
                $status = true;
            } else if (Carbon::parse($checkTglPenjualanTerakhir->created_at)->format('Y-m') == Carbon::parse($sampai)->format('Y-m')) {
                $status = true;
            } else {
                $status = false;
                $monthValidation = true;
            }

            $ts1 = strtotime($dari->format('Y-m-d'));
            $ts2 = strtotime($sampai->format('Y-m-d'));

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

            if ($diff >= 7) {
                if ($reqForecasting) {
                    $forecastingMonth = Carbon::parse($reqForecasting);

                    $forecastingFirst = Carbon::now()->startOfYear();
                    $forecastingLast = $forecastingMonth->subMonth();

                    $ts1Forecasting = strtotime($forecastingFirst->format('Y-m-d'));
                    $ts2Forcasting = strtotime($forecastingLast->format('Y-m-d'));

                    $year1Forecasting = date('Y', $ts1Forecasting);
                    $year2Forecasting = date('Y', $ts2Forcasting);

                    $month1Forecasting = date('m', $ts1Forecasting);
                    $month2Forecasting = date('m', $ts2Forcasting);

                    $diffForecasting = (($year2Forecasting - $year1Forecasting) * 12) + ($month2Forecasting - $month1Forecasting);
                } else {
                    $diffForecasting = null;
                }

                $status = true;

                if ($status) {
                    $datas->map(function($item) use ($dari, $sampai, $diff, $reqForecasting, $periodDari, $periodSampai) {
                        $forecastingMonth = Carbon::parse($reqForecasting);

                        $dari = Carbon::now()->startOfYear();
                        $sampai = $forecastingMonth->subMonth();

                        $getPenjualan = Penjualan::where('barang_id', $item->id)->whereBetween('created_at', [$dari, $sampai])->get();
                        $getPenjualanLast = Penjualan::where('barang_id', $item->id)->whereBetween('created_at', [Carbon::parse($sampai)->firstOfMonth(), Carbon::parse($sampai)->endOfMonth()])->get();

                        $item->totalQty = $getPenjualan->sum('qty');
                        $item->totalQtyLast = $getPenjualanLast->sum('qty');

                        $item->totalPenjualan = $item->harga_jual * (int)$getPenjualan->sum('qty');
                        $item->totalPenjualanLast = $item->harga_jual * (int)$getPenjualanLast->sum('qty');

                        $item->periode = $periodDari->format('F') . ' - ' . $periodSampai->format('F');

                        $getPeriod = $diff + 1;
                        $getTotalForecasting = $getPenjualan->sum('qty') / $getPeriod;
                        $item->forecasting_final = $getTotalForecasting;
                        $productSe = $getPenjualan->sum('qty') - $getTotalForecasting;
                        $item->se = pow($productSe, 2);
                    });

                    foreach ($datas as $key => $data) {
                        if ($data->totalQty == 0) {
                            unset($datas[$key]);
                        }
                    }

                    $status = true;
                    $monthValidation = true;

                    $getTotalPenjualan = $datas->sum('totalQty');
                    $getTotalPenjualanLastMonth = $datas->sum('totalQtyLast');
                    $getPeriod = $diffForecasting + 1;
                    if ($getTotalPenjualan != 0) {
                        $getTotalForecasting = $getTotalPenjualan / $getPeriod;
                    } else {
                        $getTotalForecasting = 0;
                    }

                    if ($datas->sum('se') != 0) {
                        $getMSE1 = $datas->sum('se') / $datas->count();
                    } else {
                        $getMSE1 = 0;
                    }

                    $getMSE2 = pow($getMSE1, 2);
                    $getRMSE = sqrt($getMSE1);
                }
            } else {
                $datas = [];
                $monthValidation = true;
                $status = false;
            }
        }

        return view('forecasting.index')->with([
            'datas' => $datas,
            'periode' => $diffForecasting + 1,
            'status' => $status,
            'monthValidation' => $monthValidation,
            'forecasting' => $getTotalForecasting,
            'mse' => $getMSE1,
            'rmse' => $getRMSE,
            'forecastingPermission' => $forecastingPermission
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
