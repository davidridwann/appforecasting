<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tipe;
use App\Models\Barang;
use App\Imports\BarangImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */

    protected $customMessages = [
        'required' => ':attribute Harus Diisi',
        'unique' => ':attribute Sudah Ada Didatabase',
        'max' => ':attribute Max :max Digit',
        'min' => ':attribute Minimal :min',
        'mimes' => ':attribute Harus Format .xls atau .xlsx',
    ];
    protected $customeAttr = [
        'nama' => 'Nama',
        'sku'=>'Sku',
        'harga_jual'=>'Harga Jual',
        'harga_beli'=>'Harga Beli',
        'stock'=>'Stock',
        'tipe_id'=>'Tipe',
        'file' => 'File',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['datas'] = Barang::get();
        return view('barang.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['tipes'] = Tipe::get();
        return view('barang.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'sku' => 'required|max:255',
            'harga_jual' => 'required|max:255',
            'harga_beli' => 'required|max:255',
            'stock' => 'required|max:255',
            'tipe_id' => 'required|max:255',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('nama','sku','harga_jual','harga_beli','stock','tipe_id');
        try {
            $created = Barang::create($data);
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error,'.$e->getMessage());
        }
        return redirect()->route('barang.index')->withSuccess('data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['tipes'] = Tipe::get();
        $data['barang'] = Barang::find($id);
        return view('barang.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'sku' => 'required|max:255',
            'harga_jual' => 'required|max:255',
            'harga_beli' => 'required|max:255',
            'stock' => 'required|max:255',
            'tipe_id' => 'required|max:255',
        ],$this->customMessages,$this->customeAttr);
        if ($v->fails())
        {
            $errorString = implode(",",$v->messages()->all());
            return redirect()->back()->withInput()->withError($errorString);
        }
        $data = $request->only('nama','sku','harga_jual','harga_beli','stock','tipe_id');
        $data['updated_at'] = date('Y-m-d H:i:s');
        try {
            $row = Barang::find($id);
            $row->fill($data);
            $saved = $row->save();
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('terjadi error');
        }
        return redirect()->route('barang.index')->withSuccess('data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Barang = Barang::find($id);
        $Barang->delete();
        return redirect()->route('barang.index')->withSuccess('data berhasil di hapus');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ], $this->customMessages,$this->customeAttr);

		$file = $request->file('file');

		$nama_file = rand().$file->getClientOriginalName();

		$file->move('barang',$nama_file);

        Excel::import(new BarangImport, public_path('/barang/'.$nama_file));

		return redirect()->back()->withSuccess('Berhasil import data excel');
    }
}
