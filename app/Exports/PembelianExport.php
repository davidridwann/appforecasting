<?php

namespace App\Exports;

use App\Models\Pembelian;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PembelianExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    protected $items;
    protected $keys;

    public function __construct($items, $keys) {
        $this->items = $items;
        $this->keys = $keys;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        return $this->items;
    }

    public function headings(): array
    {
        return $this->keys;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1   => ['font' => ['bold' => true]],
        ];
    }
}
