<?php

namespace App\Imports;

use App\Models\Barang;
use App\Models\Penjualan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PenjualanTokopediaImport implements ToModel, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (isset($row[10])) {
            $sku = $row[10];

            if ($sku != 'Nomor SKU') {
                $data = Barang::where('sku', $sku)->first();

                if ($data != null) {
                    $newStock = $data->stock - (int)$row[13];

                    Barang::where('id', $data->id)->update([
                        'stock' => $newStock
                    ]);

                    return new Penjualan([
                        'barang_id' => $data->id,
                        'invoice' => $row[1],
                        'harga_jual' => $row[16],
                        'qty' => $row[13],
                        'stock_sebelum' => $data->stock,
                        'stock_sesudah' => $newStock,
                        'marketplace' => 'tokopedia',
                        'type' => 'import'
                    ]);
                }
            }
        }
    }
}
