<?php

namespace App\Imports;

use App\Models\Barang;
use App\Models\Penjualan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PenjualanLazadaImport implements ToModel, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 1;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (isset($row[5])) {
            $sku = $row[5];

            if ($sku != 'sellerSku') {
                $data = Barang::where('sku', $sku)->first();

                if ($data != null) {
                    $newStock = $data->stock - 1;

                    Barang::where('id', $data->id)->update([
                        'stock' => $newStock
                    ]);

                    return new Penjualan([
                        'barang_id' => $data->id,
                        'invoice' => $row[14],
                        'harga_jual' => (int)$row[47],
                        'qty' => 1,
                        'stock_sebelum' => $data->stock,
                        'stock_sesudah' => $newStock,
                        'marketplace' => 'lazada',
                        'type' => 'import'
                    ]);
                }
            }
        }
    }
}
