<?php

namespace App\Imports;

use App\Models\Barang;
use App\Models\Penjualan;
use Maatwebsite\Excel\Concerns\ToModel;

class PenjualanShopeeImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $sku = $row[10];

        if ($sku != null) {
            if ($sku != 'SKU Induk') {
                $data = Barang::where('sku', $sku)->first();

                if ($data != null) {
                    $price = explode(' ', $row[15]);

                    $newStock = $data->stock - (int)$row[22];

                    Barang::where('id', $data->id)->update([
                        'stock' => $newStock
                    ]);

                    $separatePrice = explode('.',$price[1]);

                    $finalPrice = implode($separatePrice);

                    $penjualan = [
                        'barang_id' => $data->id,
                        'invoice' => $row[0],
                        'harga_jual' => (int)$finalPrice,
                        'qty' => $row[22],
                        'stock_sebelum' => $data->stock,
                        'stock_sesudah' => $newStock,
                        'marketplace' => 'shopee',
                        'type' => 'import'
                    ];

                    return new Penjualan($penjualan);
                }
            }
        }
    }
}
