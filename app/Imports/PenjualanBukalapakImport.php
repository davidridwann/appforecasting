<?php

namespace App\Imports;

use App\Models\Barang;
use App\Models\Penjualan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PenjualanBukalapakImport implements ToModel, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 1;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (isset($row[20])) {
            $sku = $row[20];

            if ($sku != 'SKU') {
                $data = Barang::where('sku', $sku)->first();

                if ($data != null) {
                    $newStock = $data->stock - (int)$row[19];

                    Barang::where('id', $data->id)->update([
                        'stock' => $newStock
                    ]);

                    return new Penjualan([
                        'barang_id' => $data->id,
                        'invoice' => $row[1],
                        'harga_jual' => $row[15],
                        'qty' => $row[19],
                        'stock_sebelum' => $data->stock,
                        'stock_sesudah' => $newStock,
                        'marketplace' => 'bukalapak',
                        'type' => 'import'
                    ]);
                }
            }
        }
    }
}
