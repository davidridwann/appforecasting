<?php

namespace App\Imports;

use App\Models\Barang;
use App\Models\Tipe;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BarangImport implements ToModel, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 4;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[2] != null) {
            $data = Barang::where('sku', $row[2])->first();
            
            if ($data != null) {    
                if ($row[3]) {
                    $newStock = (int)$data->stock + (int)$row[3];
                } else {
                    $newStock = (int)$data->stock;
                }
    
                Barang::where('id', $data->id)->update([
                    'stock' => $newStock
                ]);
    
            } else {
                $tipe = Tipe::where('slug', $row[6])->first();
    
                $tipeBarang = $tipe != null ? $tipe->id : null;
    
                return new Barang([
                    'nama' => $row[1],
                    'sku' => $row[2],
                    'harga_jual' => $row[4],
                    'harga_beli' => $row[5],
                    'stock' => $row[3] != null ? $row[3] : 0,
                    'tipe_id' => $tipeBarang
                ]);
            }
        }
    }
}
