<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = 'tipe';
    protected $fillable = [
        'nama', 'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function scopeSearch($query, $search){
        if ($search){
            return $query->whereRaw("(upper(nama) like '%". strtoupper($search) ."%')");
        }
    }
}
