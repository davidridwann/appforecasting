<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = 'barang';
    protected $fillable = [
        'nama','sku','harga_jual','harga_beli','stock','tipe_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function scopeSearch($query, $search){
        if ($search){
            return $query->whereRaw("(upper(nama) like '%". strtoupper($search) ."%')");
        }
    }

    public function tipe()
    {
        return $this->belongsTo("App\Models\Tipe");
    }
}
