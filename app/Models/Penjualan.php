<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = 'penjualan';
    protected $fillable = [
        'barang_id','harga_jual','qty','stock_sebelum','stock_sesudah','invoice','marketplace', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function scopeSearch($query, $search){
        if ($search){
            return $query->whereRaw("(upper(nama) like '%". strtoupper($search) ."%')");
        }
    }

    public function barang()
    {
        return $this->belongsTo("App\Models\Barang");
    }
}
