<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    public $table = 'pembelian';
    protected $fillable = [
        'barang_id','harga_beli','supplier_id','qty'
    ];

    public function barang()
    {
        return $this->belongsTo("App\Models\Barang");
    }

    public function supplier()
    {
        return $this->belongsTo("App\Models\Supplier");
    }
}
