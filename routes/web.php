<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'verify' => true,
    'reset' => true,
    'login'=>false
]);

Route::get('/login', 'Auth\LoginController@showLoginForm')->name("login");
Route::post('/login', 'Auth\LoginController@login')->name("postlogin");
Route::get('/logout', 'Auth\LoginController@logout');
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', function () {
        return redirect()->route('home');
    });
	Route::get('/','HomeController@index')->name('home');
	Route::get('/profile','HomeController@indexProfile');
	Route::post('/profile','HomeController@storeProfile');
    Route::resource('barang','BarangController');
    Route::post('barang-import','BarangController@import')->name('import.barang');
    Route::resource('supplier','SupplierController');
    Route::resource('tipe','TipeController');
    Route::resource('user','UserController');
    Route::resource('forecasting','ForecastingController');
    Route::get('laporan_persediaan','PersediaanController@index');
    Route::get('laporan_persediaan/export_persediaan', 'PersediaanController@exportExcel')->name('persediaan.export');
    Route::get('laporan_persediaan/export_persediaan/all', 'PersediaanController@exportExcelAll')->name('persediaan.export.all');
    Route::get('laporan_penjualan','PenjualanController@index')->name('penjualan.index');
    Route::get('laporan_penjualan/create','PenjualanController@create')->name('penjualan.create');
    Route::post('laporan_penjualan/store','PenjualanController@store')->name('penjualan.store');
    Route::get('laporan_penjualan/edit/{id}','PenjualanController@edit')->name('penjualan.edit');
    Route::put('laporan_penjualan/update/{id}','PenjualanController@update')->name('penjualan.update');
    Route::post('laporan_penjualan/import','PenjualanController@import')->name('laporan_penjualan.import');
    Route::resource('laporan_pembelian','PembelianController');
    Route::get('export_pembelian', 'PembelianController@exportExcel')->name('pembelian.export');
    Route::get('export_pembelian/all', 'PembelianController@exportExcelAll')->name('pembelian.export.all');

    // Permisison
    Route::get('permission','PermissionController@index');
    Route::post('permission/request','PermissionController@requestPermission')->name('permission.request');
    Route::get('permission/accept/{id}','PermissionController@acceptPermission')->name('permission.accepted');
    Route::get('permission/decline/{id}','PermissionController@declinePermission')->name('permission.declined');
});
